package com.chlwodh97.lovematchingapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false , length = 20)
    private String name;

    @Column(nullable = false , length = 13 , unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private Boolean isMan;
}
